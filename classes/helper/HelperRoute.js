const {K8} = require('@komino/k8');
const fs = require('fs');

const guardRegisterd = (hostname, reply) =>{
  //guard site registered.
  if(!fs.existsSync(`${K8.EXE_PATH}/../sites/${hostname}`)){
    reply.code(404);
    return true;
  }
  return false;
};

const resolve = async(Controller, request) => {
  try{
    const {benchmarkReset, benchmark, getBenchmarkRecords} = K8.require('DevUtils');
    benchmarkReset();
    benchmark('start');

    //import controller
    const c = new Controller(request);
    benchmark('init Controller');

    const result = await c.execute();
    benchmark('exec Controller');

    if(global.gc)gc();

    let debugText = `cache-exports:${K8.config.cache.exports}<br>`+`cache-database:${K8.config.cache.database}<br>`+`cache-view:${K8.config.cache.view}<br>`+JSON.stringify(getBenchmarkRecords().map(x => ({label: x.label, ms: x.delta})))+'<hr/>';

    debugText += '<br>' + JSON.stringify(K8.configPath) + '<hr>';

    for(let name in K8.classPath){
      debugText += `<br>${name} : ${K8.classPath[name]}`;
    }

    debugText += '<hr/>';
    for(let name in K8.viewPath){
      debugText += `<br>${name} : ${K8.viewPath[name]}`;
    }

    const matchExtension = (/\.[0-9a-z]+$/i).exec(request.raw.url || '');
    const extension = matchExtension ? matchExtension[0].replace('.', '') : 'html';

    if(extension === 'html'){
      result.body += '<div style="background-color: #000; color: #AAA; font-family: monospace; font-size: 12px; padding: 1em;">' + debugText + '</div>';
    }

    return result;

  }catch(err){
    throw err;
  }
};

module.exports = {
  resolveRoute : resolve
};